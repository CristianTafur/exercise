const htmlWebpackPlugin = require('html-webpack-plugin');
module.exports ={
    entry:'./src/Background.js',
    output:{
        path:__dirname+'/build',
        filename:'bundle.js'
    },
    plugins:[
         new htmlWebpackPlugin({
             template:'./src/index.html'
         })
    ]
};